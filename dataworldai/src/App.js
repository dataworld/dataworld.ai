import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./pages/Home";
import Header from "./components/Header";
import About from "./pages/About";
import Contact from "./pages/Contact";
import Entities from "./pages/Entities";
import Individuals from "./pages/Individuals";




function App() {
  return (
      <Router>

          <div className="app">
              <Header />
              <Switch>
                  <Route exact path="/" component={Home} />
                  <Route exact path="/about" component={About} />
                  <Route exact path="/contact" component={Contact} />
                  <Route exact path="/entities" component={Entities} />
                  <Route exact path="/individuals" component={Individuals} />
              </Switch>
          </div>




      </Router>
  );
}

export default App;

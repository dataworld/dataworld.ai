import React from "react";


import "bootstrap/dist/css/bootstrap.css";
import {Col, Container, Row} from "reactstrap";



export  class Storage extends React.Component {



    render() {

        const Box = props => <div className="box">{props.children} </div>;


        return (
            <div>
                <Container>
                    <a href="/about"><h2 className="text-center">How it Works</h2></a>
                    <Row>


                        <Col xs={{ size: 6, offset: 3 }}  >
                            <Box>
                                <p className="text-center">
                                    DataWorld was designed securely from the ground up. Personal and sensitive information
                                    is always encrypted with rotating keys, strict access model, and an audit log of each
                                    time your data is accessed. You remain in control of who has access to what, and
                                    this can be changed at any moment.
                                </p>

                            </Box>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={{ size: 6, offset: 3 }}  >
                            <Box>
                                <img src="/images/encryptionoverview.png" width="100%" />
                            </Box>
                        </Col>

                    </Row>
                </Container>
            </div>
        );
    }
}

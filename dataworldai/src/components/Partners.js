import React from "react";


import "bootstrap/dist/css/bootstrap.css";
import {Col, Container, Row} from "reactstrap";



export  class Partners extends React.Component {



    render() {

        const Box = props => <div className="box">{props.children} </div>;


        var tVendors = [{Name:"Example 1"},
            {Name:"Example 2"},
            {Name:"Example 3"},
            {Name:"Example 4"},
            {Name:"Example 5"},
            {Name:"Example 6"},
            {Name:"Example 7"},
            {Name:"Example 8"}];



        var vendorboxes = tVendors.map( (row,index) =>
            <Col xs="3" key={index} >
                <Box>
                    <div className="card">
                        <div className="card-body">
                            <h5 className="card-title">{row.Name}</h5>
                        </div>
                    </div>
                </Box>
            </Col>
        );

        return (
            <div>
            <Container>
                <h2 className="text-center">Out Partners</h2>
                <Row>
                    {vendorboxes}
                </Row>
            </Container>
            </div>
        );
    }
}

import React from "react";
import { Container, Row, Col } from "reactstrap";

import "bootstrap/dist/css/bootstrap.css";
import "../App.css";
import {Partners} from "../components/Partners";
import {Storage} from "../components/Storage";

export default class Home extends React.Component {


    render()
    {
        const Box = props => <div className="box">{props.children} </div>;

        return (
           <div>
               <div className="imgcontainer">
                   <img src="/images/world.png" />
                       <div className="imgcentered">
                           <h1>DataWorld.AI</h1>
                           <h3>Revolutionizing & Protecting Your Data</h3>
                       </div>
               </div>

               <div>
                   <Container>
                   <Row>
                       <Col xs="6">
                           <Box>
                               <h2><a href="/individuals">Individuals</a></h2>
                               <img className="homeimg" src="/images/individual.png" />
                               <p className="homep">
                                   As an Individual, you own your data, and you should have full control over your data
                                   at all times. DataWorld enables this and so much more. As more partners integrate
                                   into dataworld, the more control over all of your data you will have.
                               </p>
                           </Box>
                       </Col>

                       <Col xs="6">
                           <Box>
                               <h2><a href="/entities">Entities</a></h2>
                               <img className="homeimg" src="/images/company.png" />
                               <p className="homep">
                                   As a Business or Organizations, complying with the various privacy laws (GDPR and
                                   California Privacy Act) is becoming vital. By leveraging DataWorld, we ensure your
                                   compliance, while continuing to protect the privacy of individuals.
                               </p>
                           </Box>
                       </Col>
                   </Row>
                   </Container>
               </div>
                    <Row>
                        <Col xs="12">
                            <Box>
                                <h2><a href="/about">About DataWorld</a></h2>
                                <p className="homep">
                                    DataWorld as created with the sole purpose to protect your data. DataWorld will never
                                    sell your personal information. DataWorld enables integrations with any Entity partnering
                                    with us to centralize personal information for any number of roles they provide.
                                </p>
                            </Box>
                        </Col>
                    </Row>
               <div>

               </div>

               <Partners />



           </div>
        )
    }

}
